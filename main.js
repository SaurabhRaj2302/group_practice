$(document).ready(function () {


    $("#clearAll").click(function (e) {
        e.preventDefault();
        clearAll();
        clearForm();
    });

    $("#reset").click(function (e) {
        e.preventDefault();
        clearForm();
    });

    if (localStorage.getItem("users")) {
        var users = JSON.parse(localStorage.getItem("users"));
    }
    else {
        var users = [{ id: "row-0", fname: "saurabh", lname: "Raj", email: "singhsaurabh2302@gmail.com", mobile: 7004021112, DOB: "2021-09-15", Gender: "Male", id: "row-0" }];
    }
    if (users.length > 0) {
        generateTable();
    }


    $("#register").click(function (e) {
        e.preventDefault();
        var user = {};
        user["fname"] = $("#fname").val();
        user["lname"] = $("#lname").val();
        user["email"] = $("#email").val();
        user["mobile"] = parseInt($("#mobile").val());
        user["dob"] = $("#dob").val();
        var diff = Date.now() - new Date(user["dob"]);
        var age = Math.ceil(diff / (1000 * 3600 * 24 * 365));
        if (age < 18) {
            alert("age is less than 18 !!");
        }

        user["genderselection"] = $("#genderselection").val();

        user["id"] = users.length > 1 ? "row-" + (users.length).toString() : users.length == 1 ? "row-1" : "row-0";
        users.push(user);
        localStorage.setItem("users", JSON.stringify(users));
        generateTable();
        // console.log(users)

    });

    function clearAll() {
        users = [];
        generateTable();
        localStorage.setItem("users", JSON.stringify(users));
    }


    function clearForm() {
        $("input").val('');
    }


    function removeRow(e) {
        $(`#${e.target.id}`).closest('tr').remove();
        users = users.filter(user => user.id !== e.target.id);
        localStorage.setItem("users", JSON.stringify(users));
    }


    function generateTable() {
        document.getElementById("userTable").innerHTML = '';

        var table = document.createElement('table');

        var tHead = document.createElement('thead');
        var trHead = document.createElement('tr');

        
        ["Fist Name", "Last Name", "Email", "Mobile Number", "Date of Birth", "Gender", " Action"].forEach(col => {
            var th = document.createElement("th");
            var text = document.createTextNode(col);
            th.appendChild(text);
            trHead.appendChild(th);
        })


        tHead.appendChild(trHead);

        var tBody = document.createElement('tbody');

        users.forEach((user, index) => {
            var tr = document.createElement('tr');
            var buttonTd = document.createElement('td');
            var button = document.createElement('button');
            button.addEventListener('click', (e) => removeRow(e));
            button.setAttribute('id', `row-${index}`)
            var buttonText = document.createTextNode("Remove");
            button.appendChild(buttonText);
            buttonTd.appendChild(button);

            Object.keys(user).forEach((key) => {
                if (key === 'id')
                    return
                var td = document.createElement('td');
                var text = document.createTextNode(user[key]);
                td.appendChild(text);
                tr.appendChild(td);
                tr.appendChild(buttonTd);
            });
            tBody.appendChild(tr);
        });

        table.appendChild(tHead);
        table.appendChild(tBody);

        document.getElementById("userTable").append(table);
    }
});
